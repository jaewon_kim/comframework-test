package com.dkitec.comframeworktest.controller;

import com.dkitec.comframeworktest.entity.UserVO;
import com.dkitec.comframeworktest.repository.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/user")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    @Autowired
    UserRepository userRepository;

    @GetMapping("list")
    public List<UserVO> listUser(){
        return userRepository.findAll();
    }

    @PostMapping("create")
    public void createUser(@RequestBody UserVO userVO){
        userRepository.save(userVO);
    }

}
