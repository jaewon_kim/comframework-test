package com.dkitec.comframeworktest.service.impl;

import com.dkitec.auth.service.AuthService;
import com.dkitec.comframeworktest.controller.UserController;
import com.dkitec.core.config.properties.PwdConfig;
import com.dkitec.core.security.custom.vo.AuthLoginVO;
import com.dkitec.core.security.exception.LoginException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

@Service
public class AuthServiceImpl implements AuthService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthServiceImpl.class);
    @Override
    public AuthLoginVO loginProcess(PwdConfig pwdConfig, String userId, String userPwd, Map<String, Object> extParam) {
        AuthLoginVO vo = new AuthLoginVO();
        vo.setUserId(userId);
        return vo;
    }

    @Override
    public AuthLoginVO loginTokenProcess(String userId) {

        LOGGER.debug(userId);
        AuthLoginVO authLoginVO = new AuthLoginVO();
        authLoginVO.setUserId(userId);
        authLoginVO.setRoleList(Arrays.asList("ROLE_ADMIN"));
        return authLoginVO;
    }

    @Override
    public void loginSuccess(AuthLoginVO loginVo) {

    }

    @Override
    public void loginFail(LoginException param, PwdConfig pwdConfig) {

    }

    @Override
    public boolean isAnonymousAllow(HttpServletRequest request) {
        return false;
    }

    @Override
    public boolean authCheck(String userId, HttpServletRequest request) {
        LOGGER.debug("authCheck::userId::" + userId  + "::" + ("userId222".equals(userId)));
        return "userId222".equals(userId);
    }
}
