package com.dkitec.comframeworktest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@ComponentScan(basePackages = {"com.dkitec.comframeworktest"},
		basePackageClasses = {com.dkitec.core.config.BaseConfig.class})
@EnableAutoConfiguration
@SpringBootApplication
public class ComframeworktestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComframeworktestApplication.class, args);
	}

}
